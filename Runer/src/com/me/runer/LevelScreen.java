package com.me.runer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class LevelScreen implements Screen {
	Runer game;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	
	private Vector3 touchPoint;
	
	private BitmapFont font;
	
	Sound clickSound = Gdx.audio.newSound(Gdx.files.internal("sound/menu/click.wav"));
	Sound startGameSound = Gdx.audio.newSound(Gdx.files.internal("sound/menu/startGame.wav"));
	
	private Texture back;
	private Texture runner;
	private Texture next;
	private Texture lev1;
	private Texture lev2;
	private Texture lev3;
	
	private Rectangle backbtn;
	private Rectangle nextbtn;
	private Rectangle lev1btn;
	private Rectangle lev2btn;
	private Rectangle lev3btn;
	
	int w = Gdx.graphics.getWidth();
	int h = Gdx.graphics.getHeight();
	
	private float fontX  = 0;
	private String text1 = "Choose Level: ";
	private String text2 = "Comming Soon :)";
	
	public LevelScreen(Runer game)
	{
		this.game = game;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0 , 0 , 0 , 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		fontX = font.getBounds(text1).width/2;
		batch.begin();
		batch.draw(runner, w/2 - 370/2, 330, 370, 140);
		batch.draw(back, 0, 0, 200, 70);
		touch();
		if (Settings.commingSoonEnable == false)
		{
			font.draw(batch, text1, w/2 - fontX, 350);
			batch.draw(lev1, w/2 - 275, 120, 150, 150);
		//		batch.draw(lev2, w/2 - lev1.getWidth()/2, 300); - centruje bez zmiany rozmiary
			batch.draw(lev2, w/2 - 75, 120, 150, 150);
			batch.draw(lev3, w/2 + 125, 120, 150, 150);
			batch.draw(next, w/2 + 280, 160, 120, 75);
		}
		if (Settings.commingSoonEnable == true)
		{
			fontX = font.getBounds(text2).width/2;
			font.draw(batch, text2, w/2-fontX, h/2);
		}
		batch.end();
		

	}

	private void touch() {
		if (Gdx.input.justTouched()) {
			camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			
			if (backbtn.contains(touchPoint.x, touchPoint.y)) {
		        if (Settings.commingSoonEnable == true)
		        {
		        	clickSound.play();
		        	Settings.commingSoonEnable = false;
		        }
		        else
		        {
		        	System.out.println("Wracamy do menu :D");
		        	clickSound.play();
		        	game.setScreen(game.mainMenuScreen);
		        	return;
		        }
			}
			
			if (nextbtn.contains(touchPoint.x, touchPoint.y)) {
				if (Settings.musicEnabled == true)
				clickSound.play();
				Settings.commingSoonEnable = true;
				return;
				}
			
			if (lev1btn.contains(touchPoint.x, touchPoint.y)) {
				if (Settings.musicEnabled == true)
				startGameSound.play();
				Settings.isDead = false;
		        game.setScreen(game.level1);	
				System.out.println("Level 1 Start !!");
				return;
				}
			if (lev2btn.contains(touchPoint.x, touchPoint.y)) {	
				System.out.println("Level 2 Start !!");
				return;
				}
			if (lev3btn.contains(touchPoint.x, touchPoint.y)) {
				if (Settings.musicEnabled == true)
				startGameSound.play();
				Settings.isDead = false;
		        game.setScreen(game.level3);	
				System.out.println("Level 3 Start !!");
				return;
				}
		} 
	}

	@Override
	public void show() {
		Settings.playingNow = false;
		batch = new SpriteBatch();
		touchPoint = new Vector3();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, w, h);
		
		font = new BitmapFont(Gdx.files.internal("font/font.fnt"));
	    
		lev1 = new Texture(Gdx.files.internal("menu/lvl1.png"));
		lev2 = new Texture(Gdx.files.internal("menu/lvl2.png"));
		lev3 = new Texture(Gdx.files.internal("menu/lvl3.png"));
		back = new Texture(Gdx.files.internal("menu/back.png"));
		next = new Texture(Gdx.files.internal("menu/next.png"));
		runner = new Texture(Gdx.files.internal("menu/runner.png"));
		
		lev1btn = new Rectangle(w/2 - 275, 120, 150, 150);
		lev2btn = new Rectangle(w/2 - 75, 120, 150, 150);
		lev3btn = new Rectangle(w/2 + 125, 120, 150, 150);
		backbtn = new Rectangle(0, 0, 200, 70);
		nextbtn = new Rectangle(w/2 + 280, 160, 120, 75);
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		batch.dispose();
		clickSound.dispose();
		next.dispose();
		lev1.dispose();
		lev2.dispose();
		lev3.dispose();
		startGameSound.dispose();
		runner.dispose();
		font.dispose();
	}
	
	

}
