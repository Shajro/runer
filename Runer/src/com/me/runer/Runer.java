package com.me.runer;


import com.badlogic.gdx.Game;
import com.me.levels.Level1;
import com.me.levels.Level3;


public class Runer extends Game {

	LoadScreen loadScreen;
	LevelScreen levelScreen;
	public MainMenuScreen mainMenuScreen;
	Level1 level1;
	Level3 level3;
	OptionsScreen optionsScreen;
	public AfterScreen afterScreen;
	
	public void create() {		
		loadScreen = new LoadScreen(this);
		levelScreen = new LevelScreen(this);
		mainMenuScreen = new MainMenuScreen(this);
		level1 = new Level1(this);
		level3 = new Level3(this);
		optionsScreen = new OptionsScreen(this);
		afterScreen = new AfterScreen(this);
		setScreen(loadScreen);
	}

	@Override
	public void dispose() {

	}

	@Override
	public void render() {		
		super.render();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	public void render(float delta) {
		// TODO Auto-generated method stub
		
	}

	public void show() {
		// TODO Auto-generated method stub
		
	}

	public void hide() {
		// TODO Auto-generated method stub
		
	}
}
