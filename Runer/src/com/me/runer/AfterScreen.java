package com.me.runer;

import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class AfterScreen implements Screen {

	Runer game;
	
	MainMenuScreen mainMenuScreen;
	
	SpriteBatch batch;
	
	OrthographicCamera camera;
	
	Texture dead;
	Texture alive;
	
	BitmapFont font;
	
	int w = Gdx.graphics.getWidth();
	int h = Gdx.graphics.getHeight();
	
	Sound clickSound = Gdx.audio.newSound(Gdx.files.internal("sound/menu/click.wav"));
	Music levelFinish = Gdx.audio.newMusic(Gdx.files.internal("sound/game/finishLevel.wav"));
	Music gameOverMusic = Gdx.audio.newMusic(Gdx.files.internal("sound/game/endGameSound.mp3"));
	
	
    public AfterScreen(Runer game)
    {
        this.game = game;
    }

	@Override
	public void render(float delta) {

		Texture.setEnforcePotImages(false);
		
		Gdx.gl.glClearColor(0 , 0 , 0 , 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		

		
		batch.begin();
		if (Settings.isDead == true)
		{
		batch.draw(dead, 0, 0);
		font.draw(batch,"Your score is:" + String.valueOf(Hud.score),230,100);
		}
		if (Settings.isDead == false)
		{
		batch.draw(alive, 0, 0);
		font.draw(batch,"Your score is:" + String.valueOf(Hud.score), 230,100);
		}
		batch.end();
		
		if (Gdx.input.isTouched())
		{
			if (Settings.soundEnabled == true)
			clickSound.play();
			game.setScreen(game.mainMenuScreen);
			gameOverMusic.stop();
			levelFinish.stop();
			Settings.isDead = false;
		}
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		Settings.playingNow = false;
		
		if (Settings.isDead == false && Settings.musicEnabled == true)
		levelFinish.play();
		if (Settings.isDead == true && Settings.musicEnabled == true)
		gameOverMusic.play();
		
		batch = new SpriteBatch();
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false, w, h);
		
		dead = new Texture(Gdx.files.internal("game/gameOver.png"));
		alive = new Texture(Gdx.files.internal("game/winPicture.png"));

		font = new BitmapFont(Gdx.files.internal("font/font.fnt"));

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		gameOverMusic.dispose();
		clickSound.dispose();
		dead.dispose();
		batch.dispose();
		alive.dispose();
		levelFinish.dispose();
		font.dispose();

	}
    
}
