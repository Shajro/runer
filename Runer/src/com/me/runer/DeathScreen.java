package com.me.runer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class DeathScreen implements Screen {

	Runer game;
	
	MainMenuScreen mainMenuScreen;
	
	SpriteBatch batch;
	
	OrthographicCamera camera;
	
	Texture dead;
	
	int w = Gdx.graphics.getWidth();
	int h = Gdx.graphics.getHeight();
	
	Sound clickSound = Gdx.audio.newSound(Gdx.files.internal("sound/menu/click.wav"));
	Music gameOverMusic = Gdx.audio.newMusic(Gdx.files.internal("music/gameOver.wav"));
	
	
    public DeathScreen(Runer game)
    {
        this.game = game;
    }

	@Override
	public void render(float delta) {
		Texture.setEnforcePotImages(false);
		
		Gdx.gl.glClearColor(0 , 0 , 0 , 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		gameOverMusic.play();
		
		batch.begin();
		batch.draw(dead, 0, 0);
		batch.end();
		
		if (Gdx.input.isTouched())
		{
			clickSound.play();
			Settings.isDead = false;
			game.setScreen(game.mainMenuScreen);
			gameOverMusic.stop();
		}
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		Settings.isDead = true;
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, w, h);
		dead = new Texture(Gdx.files.internal("game/gameOver.png"));


	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		gameOverMusic.dispose();
		clickSound.dispose();
		dead.dispose();
		batch.dispose();

	}
    
}
