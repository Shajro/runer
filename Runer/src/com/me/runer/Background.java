package com.me.runer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Background {
	
	private Runer game;
	private OrthographicCamera cameraBackground;
	private OrthographicCamera cameraFormationsBack;
	private OrthographicCamera cameraFormationsFront;
    private SpriteBatch batch;
    private Texture imgBackground;
    private Texture imgFormationsBack;
    private Texture imgFormationsFront;

    public Background (Runer game)
	{
		this.game = game;
		
		cameraBackground = new OrthographicCamera();
        cameraBackground.setToOrtho(false);
        cameraBackground.position.x = 400;
        cameraBackground.position.y = 300;
        
        cameraFormationsBack = new OrthographicCamera();
        cameraFormationsBack.setToOrtho(false);
        cameraFormationsBack.position.x = 400;
        cameraFormationsBack.position.y = 300;
        
        cameraFormationsFront = new OrthographicCamera();
        cameraFormationsFront.setToOrtho(false);
        cameraFormationsFront.position.x = 400;
        cameraFormationsFront.position.y = 300;
        
        batch = new SpriteBatch();
        
        imgBackground = new Texture(Gdx.files.internal("game/background.png"));
        imgFormationsBack = new Texture(Gdx.files.internal("game/formations_back.png"));
        imgFormationsFront = new Texture(Gdx.files.internal("game/formations_front.png"));
		
	}
	
	public void render(float mainCameraX)
	{
		draw(imgBackground, cameraBackground, (float) (0.2*mainCameraX));
		draw(imgFormationsBack, cameraFormationsBack, (float) (0.3*mainCameraX));
		draw(imgFormationsFront, cameraFormationsFront, (float) (0.6*mainCameraX));
	}
	
	private void draw(Texture img, OrthographicCamera camera, float x)
	{
	    camera.position.set(x, camera.position.y, 0);
        
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        
        batch.begin();
        
        batch.draw(img, ((int) x/img.getWidth() - 1)*img.getWidth(), 0);
        batch.draw(img, ((int) x/img.getWidth())*img.getWidth(), 0);
        batch.draw(img, ((int) x/img.getWidth() + 1)*img.getWidth(), 0);
        
        batch.end();
	}
}