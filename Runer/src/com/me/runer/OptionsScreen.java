package com.me.runer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class OptionsScreen implements Screen {
	
    Runer game;
    BitmapFont bfont;
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Texture runner;
    private Texture back;
    private Rectangle backbtn;
    private Rectangle musicbtn;
    private Rectangle soundbtn;
    Vector3 touchPoint;
    
    float w = Gdx.graphics.getWidth();
    float h = Gdx.graphics.getHeight();
    
	Sound clickSound = Gdx.audio.newSound(Gdx.files.internal("sound/menu/click.wav"));
	Sound startGameSound = Gdx.audio.newSound(Gdx.files.internal("sound/menu/startGame.wav"));

	private float fontX  = 0;
	
	private String text1 = "Music On";
	private String text2 = "Music Off";;
	private String text3 = "Sound On";;
	private String text4 = "Sound Off";
	
	
    
	public OptionsScreen(Runer game){
        this.game = game;
}

	@Override
	public void show() {
	    
		runner = new Texture(Gdx.files.internal("menu/runner.png"));
		back = new Texture(Gdx.files.internal("menu/back.png"));
		backbtn = new Rectangle(0, 0, 200, 70);
		musicbtn = new Rectangle(w/2 - 250/2, 230,200,50);
		soundbtn = new Rectangle(w/2 - 250/2, 160,200,50);
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false, w, h);
		
		bfont = new BitmapFont(Gdx.files.internal("font/font.fnt"));
		batch = new SpriteBatch();
		
		touchPoint = new Vector3();
		
	}

	@Override
	public void render(float delta) {
		
		Gdx.gl.glClearColor(0 , 0 , 0 , 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		

					
		batch.setProjectionMatrix(camera.combined);
		
		batch.begin();
		batch.draw(back, 0, 0, 200, 70);
		batch.draw(runner, w/2 - 370/2, 330, 370, 140);
		//batch.draw(test, w/2 - 250/2, 160,200,50);
		checkState();
		batch.end();
		if (Gdx.input.justTouched()) {
			camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			
			if (backbtn.contains(touchPoint.x, touchPoint.y)) {
				System.out.println("Wracamy do menu :D");
				if (Settings.soundEnabled == true)
				clickSound.play();
		        game.setScreen(game.mainMenuScreen);	
				return;
				}
			if (musicbtn.contains(touchPoint.x, touchPoint.y)) {
				if (Settings.soundEnabled == true)
				clickSound.play();
				System.out.println("muza powerrr :D");
				checkMusic();

				System.out.println(Settings.musicEnabled);
				return;
				}
			if (soundbtn.contains(touchPoint.x, touchPoint.y)) {
				if (Settings.soundEnabled == true)
				clickSound.play();
				System.out.println("dzwieki powerr :D");
				checkSound();
				System.out.println(Settings.soundEnabled);
				return;
				}
		}
	}

	private void checkSound() 
	{		
		if (Settings.soundEnabled == true)
		Settings.soundEnabled = false;
		else
		Settings.soundEnabled = true;	
	}

	private void checkMusic() {
		if (Settings.musicEnabled == true)
		Settings.musicEnabled = false;
		else
		Settings.musicEnabled = true;
		
	}

	private void checkState() {
		if (Settings.musicEnabled == true)
		{
				fontX = bfont.getBounds(text1).width/2;
				bfont.draw(batch, text1, w/2 - fontX, 270);
		}
	    if (Settings.musicEnabled == false)
	    {
				fontX = bfont.getBounds(text2).width/2;;
				bfont.draw(batch, text2, w/2 - fontX, 270);
	    }

		if (Settings.soundEnabled == true)
			{
				fontX = bfont.getBounds(text3).width/2;
				bfont.draw(batch, text3, w/2 - fontX, 200);
			}
	    if (Settings.soundEnabled == false)
			{
				fontX = bfont.getBounds(text4).width/2;
	    		bfont.draw(batch, text4, w/2 - fontX, 200);
	    	}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		batch.dispose();
		clickSound.dispose();
		startGameSound.dispose();
		back.dispose();

		runner.dispose();		
	}

}
