package com.me.runer;

import sun.font.TrueTypeFont;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class MainMenuScreen implements Screen {
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Texture runner;
	private Texture start;
	private Texture options;
	private Texture exit;
	private Texture continueTexture;
	private BitmapFont font;
	Vector3 touchPoint;
	
	Rectangle startbtn;
	Rectangle optionsbtn;
	Rectangle exitbtn;	
	Rectangle backbtn;
	Rectangle debugBtn;
	Rectangle continueBtn;
	
    float w = Gdx.graphics.getWidth();
    float h = Gdx.graphics.getHeight();
    
    private int pom = 0; //przesuniecie napisow w menu w wypadku  w��czonej gry
    private int currentLevel = 0; //przy w��czonej grze wskazuje kt�ry lvl zosta� w��czony, aby mozna bylo do niego wrocic bez resetu ustawien
    

	Sound clickSound = Gdx.audio.newSound(Gdx.files.internal("sound/menu/click.wav"));
	Sound startGameSound = Gdx.audio.newSound(Gdx.files.internal("sound/menu/startGame.wav"));
    Music menuMusic = Gdx.audio.newMusic(Gdx.files.internal("music/menuMusic1.mp3"));
		
	Runer game;
	
	MainMenuScreen mainMenuScreen;
	
	
	
    public MainMenuScreen(Runer game){
        this.game = game;

}

	@Override
	public void show() {
		//Omijanie pot�gi 2
		Texture.setEnforcePotImages(false);


		
		camera = new OrthographicCamera();
		camera.setToOrtho(false, w, h);
		batch = new SpriteBatch();
		startbtn = new Rectangle(w/2 - 250/2, 230-pom, 250, 85);
		optionsbtn = new Rectangle(w/2 - 250/2, 150-pom, 250, 85);
		exitbtn = new Rectangle(w/2 - 250/2, 70-pom, 250, 85);
		debugBtn = new Rectangle(0, 0, 350, 70);
		backbtn = new Rectangle(0, 0, 200, 70);
		continueBtn = new Rectangle(w/2 - 250/2, 0, 300, 70);
		
		//menuMusic.play();
		
		
		
		touchPoint = new Vector3();
		
		runner = new Texture(Gdx.files.internal("menu/runner.png"));
		start = new Texture(Gdx.files.internal("menu/newGame.png"));
		options = new Texture(Gdx.files.internal("menu/options.png"));
		exit = new Texture(Gdx.files.internal("menu/exit.png"));
		continueTexture = new Texture(Gdx.files.internal("menu/continue.png"));
		font = new BitmapFont(Gdx.files.internal("font/font.fnt"));
		


	}

	@Override
	public void render(float delta) {
		
			
	
			Gdx.gl.glClearColor(0 , 0 , 0 , 0);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
						
			batch.setProjectionMatrix(camera.combined);
			batch.begin();
			batch.draw(runner,w/2 - 370/2, 330, 370, 140);
			if (Settings.playingNow == true)
			{
			  pom = 35;
				batch.draw(continueTexture,w/2 - 250/2, 310-pom, 250, 85);
			}
			else
				pom = 0;
			touch();
			batch.draw(start,w/2 - 250/2, 230-pom, 250, 85);
			batch.draw(options,w/2 - 250/2, 150-pom, 250, 85);
			batch.draw(exit,w/2 - 250/2, 70-pom, 250, 85);
			dbgMod();
			dbgState();

			batch.end();



	}

	private void dbgState() {
		if (Gdx.input.justTouched()) {
			camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			{
				if (debugBtn.contains(touchPoint.x, touchPoint.y)) {
					if (Settings.debugModeOn == true)
						Settings.debugModeOn = false;
					else
						Settings.debugModeOn = true;
				}
	
			}
		}
	}

	private void dbgMod() {
		font.setColor(Color.WHITE);
		if (Settings.debugModeOn == false)
		font.draw(batch, "Debug Mode Off", 0, 40);
		else
		font.draw(batch, "Debug Mode On", 0, 40);
		}

	private void touch() {
		if (Gdx.input.justTouched()) {
			camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			if (Settings.creditsEnable == false)
			{
				if (startbtn.contains(touchPoint.x, touchPoint.y)) {
					System.out.println("Start dziala :D");
					if (Settings.soundEnabled == true)
					startGameSound.play();
					game.setScreen(game.levelScreen);	
					return;
					}
				if (optionsbtn.contains(touchPoint.x, touchPoint.y)) {
					if (Settings.soundEnabled == true)
					clickSound.play();
					game.setScreen(game.optionsScreen);	
					System.out.println("opcje dziala :D");
					return;
					}

				if (exitbtn.contains(touchPoint.x, touchPoint.y)) {
					if (Settings.soundEnabled == true)
					clickSound.play();
					System.out.println("c�, bye XD");
					Gdx.app.exit();
					return;
					}
				}
		}
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		batch.dispose();
		clickSound.dispose();
		start.dispose();
		options.dispose();
		exit.dispose();
		startGameSound.dispose();
		runner.dispose();
		font.dispose();
	}

}
