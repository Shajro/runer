package com.me.runer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LoadScreen implements Screen {
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Texture logo;
	private Texture helper;
	private Texture logo2;

	private long time;
	Runer game;
	
    public LoadScreen(Runer game)
    {
        this.game = game;
    }
    
	@Override
	public void render(float delta) {
		long actTime = System.currentTimeMillis() - time;
		System.out.println(actTime);
		
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		Gdx.gl.glClearColor(1 , 1 , 1 , 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.draw(logo, w/2 - 450/2, 11, 450, 450);
		if (actTime > 2000)
		{
			batch.draw(helper, 0, 0, w, h);
			batch.draw(logo2, w/2 - logo2.getWidth()/2, h/2 - logo2.getHeight()/2);
		}
		batch.end();
		
		if (actTime > 4000)
        game.setScreen(game.mainMenuScreen);		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void show() {
		Texture.setEnforcePotImages(false);
		
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false, w, h);
		batch = new SpriteBatch();
		
		logo = new Texture(Gdx.files.internal("logo.png"));
		helper = new Texture(Gdx.files.internal("helper.png"));
		logo2 = new Texture(Gdx.files.internal("logo2.png"));
		
		time = System.currentTimeMillis();
	}
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void dispose() {
		batch.dispose();
		helper.dispose();
		logo.dispose();
		logo2.dispose();
		

	}
}
