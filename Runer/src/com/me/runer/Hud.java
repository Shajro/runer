package com.me.runer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.me.levels.Level3;

public class Hud implements Screen {
	
	MainMenuScreen mainMenuSceen;
	
	Bob bob;
	
	Level3 level3;
	
	Runer game;
	
    float w = Gdx.graphics.getWidth();
    float h = Gdx.graphics.getHeight();
	
	private Texture pauseImg;
	private Texture jumpImg;
	private Texture debugText;
	
	private OrthographicCamera Hud_camera;
	private SpriteBatch batch_hud;
	
	private Rectangle pauseRect;
	private Rectangle jumpRect;

	Vector3 touchPoint;
	
	private BitmapFont font;
	
	public static int score = 0;
	private float fontX  = 0;
	private float fontY = 0;
	private String text = "Score: " + String.valueOf(score);
	
	public Hud (Runer game)
	{
	    bob = new Bob(game);
		mainMenuSceen = new MainMenuScreen(game);
		Texture.setEnforcePotImages(false);
		font = new BitmapFont(Gdx.files.internal("font/font.fnt"));
		pauseImg = new Texture(Gdx.files.internal("gui/pauseImg.png"));
		jumpImg = new Texture(Gdx.files.internal("gui/jumpImg.png"));
		debugText = new Texture(Gdx.files.internal("test.png"));
		
		pauseRect = new Rectangle(0, h-120, 120, 120);
		jumpRect = new Rectangle(w-120, 0, 120, 120);
		
		batch_hud = new SpriteBatch();
		
		touchPoint = new Vector3();
		
		Hud_camera = new OrthographicCamera();
    	Hud_camera.setToOrtho(false);
	
	}
	    
	@Override
	public void render(float delta) {
		fontX = font.getBounds(text).width/2;
		fontY = font.getBounds(text).height/2;
	    Hud_camera.update();

	    batch_hud.begin();
	    if(Settings.debugModeOn == true)
	    {
			batch_hud.draw(debugText, 0, h-120, 120, 120);
		    batch_hud.draw(debugText, w-120, 0, 120, 120);
	    }
	    batch_hud.setProjectionMatrix(Hud_camera.combined);
	    font.setColor(Color.BLACK);
	    //po wycentrowniu skore sie buguje, nie uaktualnia sie na bierz�co :(
	    font.draw(batch_hud,Integer.toString(score), w/2-fontX ,h-fontY);

		batch_hud.draw(pauseImg, 0, h-120, 120, 120);
	    batch_hud.draw(jumpImg, w-120, 0, 120, 120);
	    batch_hud.end();
	    touch();
		if (Gdx.input.isKeyPressed(Keys.C))
		{
			System.out.println("x = " + touchPoint.x);
			System.out.println("y = " + touchPoint.y);
		}
		
	}

	private void touch() {
		if (Gdx.input.isTouched())
		{	
			Hud_camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			//System.out.println("x = " + touchPoint.x);
			//System.out.println("y = " + touchPoint.y);
			if (pauseRect.contains(touchPoint.x, touchPoint.y)) {
				System.out.println("Menu??");
				level3.lelelel = 1;
				}
			if (jumpRect.contains(touchPoint.x, touchPoint.y)) {
				bob.isJumping = true;
				System.out.println("jummp??");
				  if (bob.isJumping == true)
				  {
					  bob.playerY = bob.playerY + bob.gravity;
					  if (bob.playerY >= bob.maxJump)
					  {
						  bob.isFalling = true;
						  bob.isJumping = false;
					  }
					  System.out.println("bob.isFalling = " + bob.isFalling);
					  System.out.println(" bob.isJumping = " +  bob.isJumping);
					  System.out.println("touchPoint.x = " + touchPoint.x);
				  }
			}
		}
		}
				  /*
			else if (bob.isFalling == true)
			{
				bob.playerY = bob.playerY - bob.gravity;
				if (bob.playerY == 0)
				{
					bob.isJumping = true;
					bob.isFalling = false;
					//isGrounded = true;
				}
			} 
			}
			}*/


	public void dispose()
	{
		pauseImg.dispose();
		jumpImg.dispose();
		batch_hud.dispose();
	}

				@Override
				public void resize(int width, int height) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void show() {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void hide() {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void pause() {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void resume() {
					// TODO Auto-generated method stub
					
				}
}