package com.me.levels;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.me.runer.Background;
import com.me.runer.Bob;
import com.me.runer.Hud;
import com.me.runer.MainMenuScreen;
import com.me.runer.Runer;
import com.me.runer.Settings;

public class Level1 implements Screen {
	Runer game;
	Bob bob;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	
    float w = Gdx.graphics.getWidth();
    float h = Gdx.graphics.getHeight();
    

    

    
	public Rectangle pauseBtn;
	public Rectangle jumpBtn;
	public Rectangle endGameCol;
	public Rectangle terrain;
	public Rectangle testowy;
	public Rectangle coin1Col;
	public Rectangle coin2Col;
	public Rectangle coin3Col;
	public Rectangle coin4Col;
	public Rectangle playerCol;
	
	public BitmapFont font;
    
	private Texture player;

	private Texture test;
	private Texture endGameText;
	private Texture coin1;
	private Texture coin2;

	
    private float elapsedTime = 0;
    public int coinX = 0;
    public int random = 0;
    
    private Random rand;
	
    private TextureAtlas textureAtlas;
    private Animation animation;
	
	Vector3 touchPoint;
    	
	public Hud hud;
	public Background background;
	

//	private boolean isGrounded = true;
	
//	MainMenuScreen mainMenuSceen;
	
    public Level1(Runer game){
        this.game = game;
        hud = new Hud(game);
        bob = new Bob(game);
        background = new Background(game);
       // mainMenuSceen = new MainMenuScreen(game);
    }

	@Override
	public void render(float delta) {
		Settings.playingNow = true;
		playerCol.x = bob.playerX - 300;
		playerCol.y = bob.playerY;
		Gdx.gl.glClearColor(1 , 1 , 1 , 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
					
		batch.setProjectionMatrix(camera.combined);
		camera.setToOrtho(false, w, h);
		
     	elapsedTime += Gdx.graphics.getDeltaTime();

		camera.position.x = bob.playerX;
		background.render(camera.position.x);
		camera.update();
		batch.begin();
		//if (Gdx.input.isKeyPressed(Keys.D))
		bob.playerX = bob.playerX + bob.playerSpeed;
		batch.draw(animation.getKeyFrame(elapsedTime, true), bob.playerX - 300, bob.playerY,110,127);
		if (Settings.debugModeOn == true)
		{
		batch.draw(test, terrain.x, terrain.y, terrain.width, terrain.height);
		batch.draw(test, coin1Col.x, coin1Col.y,30,30);
		batch.draw(test, coin2Col.x, coin2Col.y,30,25);
		batch.draw(test, coin3Col.x, coin3Col.y,30,25);
		batch.draw(test, coin4Col.x, coin4Col.y,30,30);
		batch.draw(test, w-120, 0, 120, 120);
		}
		batch.draw(coin1, coin1Col.x, coin1Col.y,30,30);
		
		batch.draw(coin2, coin2Col.x, coin2Col.y,30,25);
		batch.draw(coin2, coin3Col.x, coin3Col.y,30,25);
		batch.draw(coin1, coin4Col.x, coin4Col.y,30,30);
		batch.draw(endGameText, 4000,20);
	    jump();	
		drawRandCoins();
		collisionDetect();



		batch.end();
		hud.render(delta);
		if (Gdx.input.isKeyPressed(Keys.W))
		bob.playerSpeed += 1;
		if (Gdx.input.isKeyPressed(Keys.Q))
			bob.playerSpeed -= 1;
		if (Gdx.input.isKeyPressed(Keys.F))
		game.setScreen(game.mainMenuScreen);
	}

	private void collisionDetect() {
		if (playerCol.overlaps(terrain))
			Hud.score = Hud.score + 5;
		if (playerCol.overlaps(coin1Col))
			Hud.score = Hud.score + 13;
		if (playerCol.overlaps(coin2Col))
			Hud.score = Hud.score + 23;
		if (playerCol.overlaps(coin3Col))
			Hud.score = Hud.score + 33;
		if (playerCol.overlaps(coin4Col))
			Hud.score = Hud.score + 43;
		if (playerCol.overlaps(endGameCol))
		{
			Settings.playingNow = false;
			game.setScreen(game.afterScreen);
			bob.playerX = 0;
		}
	}

	private void jump() {
		if (Gdx.input.isTouched())
		{	
			camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			if (jumpBtn.contains(touchPoint.x, touchPoint.y)) {
			if (bob.isJumping == true)
			{
				bob.playerY = bob.playerY + bob.gravity;
				if (bob.playerY >= bob.maxJump)
				{
					bob.isFalling = true;
					bob.isJumping = false;
				}
			}
			
			else if (bob.isFalling == true)
			{
				bob.playerY = bob.playerY - bob.gravity;
				if (bob.playerY == 0)
				{
					bob.isJumping = true;
					bob.isFalling = false;
					//isGrounded = true;
				}
			}
			}
		}
	}

	private void drawRandCoins() {
		int coinY;
		int random = 0;
		int maxCoins = 5;
		
		int emptySpaceMin = 20;
		
		int maxHeight = Gdx.graphics.getHeight() - 600;
		
		coinX = coinX + random;
	
				if (maxCoins > 4)
				{
				maxCoins = maxCoins + 1;
			 	emptySpaceMin += 20;
				random= rand.nextInt(50);
				batch.draw(coin1, emptySpaceMin + coinX, 20,35,35);				
				}
	}
	
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void show() {
		Texture.setEnforcePotImages(false);
		camera = new OrthographicCamera();
		camera.setToOrtho(false, w, h);
		
		pauseBtn = new Rectangle(0, h-120, 120, 120);
		jumpBtn = new Rectangle (w-120, 0, 120, 120);
		
		//mainMenuSceen = new MainMenuScreen(game);
		
		rand = new Random();
		//						 x  y   w   h
		terrain = new Rectangle(500,20,500,120);
		playerCol = new Rectangle(0,0,110,127);
		coin1Col = new Rectangle(400,60,35,25);
		coin2Col = new Rectangle(1200,33,35,30);
		coin3Col = new Rectangle(1300,0,35,30);
		coin4Col = new Rectangle(1799,130,25,25);
		endGameCol = new Rectangle(4000,20,50, 50);
	//	testowy = new Rectangle()
		font = new BitmapFont();
		textureAtlas = new TextureAtlas(Gdx.files.internal("player/ram_run/runpack.atlas"));
		animation = new Animation(1/15f, textureAtlas.getRegions());
		
		player = new Texture(Gdx.files.internal("player/ram_idle/ram_idle0001.png"));
		test = new Texture(Gdx.files.internal("test.png"));
		coin1 = new Texture(Gdx.files.internal("game/coin.png"));
		coin2 = new Texture(Gdx.files.internal("game/coin2.png"));
		endGameText = new Texture(Gdx.files.internal("game/end.png"));
		
		touchPoint = new Vector3();
		
		batch = new SpriteBatch();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub	
	}

	@Override
	public void dispose() {
		batch.dispose();
		font.dispose();
		player.dispose();
		test.dispose();
		coin1.dispose();
		coin2.dispose();
		endGameText.dispose();
		textureAtlas.dispose();
		hud.dispose();
		
	}
}