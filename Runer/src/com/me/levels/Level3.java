package com.me.levels;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.me.runer.Background;
import com.me.runer.Bob;
import com.me.runer.Hud;
import com.me.runer.MainMenuScreen;
import com.me.runer.Runer;
import com.me.runer.Settings;

public class Level3 implements Screen {
	Runer game;
	Bob bob;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	
    float w = Gdx.graphics.getWidth();
    float h = Gdx.graphics.getHeight();
    
    
	public Rectangle pauseBtn;
	public Rectangle jumpBtn;
	public Rectangle fireCol;
	public Rectangle testowy;
	public Rectangle playerCol;
	
	public BitmapFont font;
	
	public int lelelel = 0;
    
	private Texture player;

	private Texture test;
	private Texture fireText;


	
    private float elapsedTime = 0;
    public int coinX = 0;
    public int random = 0;
	
    private TextureAtlas textureAtlas;
    private Animation animation;
	
	Vector3 touchPoint;
    	
	public Hud hud;
	public Background background;
	

//	private boolean isGrounded = true;
	
//	MainMenuScreen mainMenuSceen;
	
    public Level3(Runer game){
        this.game = game;
        hud = new Hud(game);
        bob = new Bob(game);
        background = new Background(game);
       // mainMenuSceen = new MainMenuScreen(game);
    }

	@Override
	public void render(float delta) {
		Settings.playingNow = true;
		playerCol.x = bob.playerX - 300;
		playerCol.y = bob.playerY;
		Gdx.gl.glClearColor(1 , 1 , 1 , 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
					
		batch.setProjectionMatrix(camera.combined);
		camera.setToOrtho(false, w, h);
		
     	elapsedTime += Gdx.graphics.getDeltaTime();

		camera.position.x = bob.playerX;
		background.render(camera.position.x);
		camera.update();
		batch.begin();
		//if (Gdx.input.isKeyPressed(Keys.D))
		bob.playerX = bob.playerX + bob.playerSpeed;
		batch.draw(animation.getKeyFrame(elapsedTime, true), bob.playerX - 300, bob.playerY,110,127);
		if (Settings.debugModeOn == true)
		{
		batch.draw(test, w-120, 0, 120, 120);
		}
		batch.draw(fireText, 400,0,300,331);
	    jump();	
		collisionDetect();



		batch.end();
		hud.render(delta);
		if (Gdx.input.isKeyPressed(Keys.W))
		bob.playerSpeed += 1;
		if (Gdx.input.isKeyPressed(Keys.Q))
			bob.playerSpeed -= 1;
		if (Gdx.input.isKeyPressed(Keys.F))
		game.setScreen(game.mainMenuScreen);
		if (lelelel == 1)
		{
			lelelel =0;
			game.setScreen(game.mainMenuScreen);
		}

	}

	private void collisionDetect() {
		if (playerCol.overlaps(fireCol))
		{
			Settings.playingNow = false;
			Settings.isDead = true;
			game.setScreen(game.afterScreen);
		}
	}

	private void jump() {
		if (Gdx.input.isTouched())
		{	
			camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			if (jumpBtn.contains(touchPoint.x, touchPoint.y)) {
			if (bob.isJumping == true)
			{
				bob.playerY = bob.playerY + bob.gravity;
				if (bob.playerY >= bob.maxJump)
				{
					bob.isFalling = true;
					bob.isJumping = false;
				}
			}
			
			else if (bob.isFalling == true)
			{
				bob.playerY = bob.playerY - bob.gravity;
				if (bob.playerY == 0)
				{
					bob.isJumping = true;
					bob.isFalling = false;
					//isGrounded = true;
				}
			}
			}
		}
	}

	
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void show() {
		Texture.setEnforcePotImages(false);
		camera = new OrthographicCamera();
		camera.setToOrtho(false, w, h);
		
		pauseBtn = new Rectangle(0, h-120, 120, 120);
		jumpBtn = new Rectangle (w-120, 0, 120, 120);
		
		//mainMenuSceen = new MainMenuScreen(game);

		//						 x  y   w   h
		playerCol = new Rectangle(0,0,110,127);
		fireCol = new Rectangle(400,0,300,331);
		font = new BitmapFont();
		textureAtlas = new TextureAtlas(Gdx.files.internal("player/ram_run/runpack.atlas"));
		animation = new Animation(1/15f, textureAtlas.getRegions());
		
		player = new Texture(Gdx.files.internal("player/ram_idle/ram_idle0001.png"));
		test = new Texture(Gdx.files.internal("test.png"));
		fireText = new Texture(Gdx.files.internal("game/fire.png"));
		
		touchPoint = new Vector3();
		
		batch = new SpriteBatch();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub	
	}

	@Override
	public void dispose() {
		batch.dispose();
		font.dispose();
		player.dispose();
		test.dispose();
		fireText.dispose();
		textureAtlas.dispose();
		hud.dispose();
		
	}
}